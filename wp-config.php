<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '0{BuIp<kQBVV$6T@a!4<5LK*9Yq_<dfPPRSs=.m2Ci1m{3O;/Oa-}rWwms(%rd3`' );
define( 'SECURE_AUTH_KEY',  'uu>o$B,mX{.A_Cgb+(Q;}avPstE .hggo6o.@-|Uz i]IK<dv2y]gOM:s(nd[/36' );
define( 'LOGGED_IN_KEY',    '2wi!,prN>xr{x>sEzdtUH`?r88SizZv0Sjs.Vi%R]p1,.^=D2=axIu7;o:jn4rq|' );
define( 'NONCE_KEY',        '~ AJN`[*r(r4B+8NDem3%u}Z$#>ivi WluLcLSa7NA~en:/F#,4X_)V5M{L{dlo<' );
define( 'AUTH_SALT',        '<y8j[GnEi>EJ_]P>^8W*Tg39_8C:@S23I/6$At)SNLo+5q!:y=cJh9m7qn1mN*0g' );
define( 'SECURE_AUTH_SALT', 'Wcx^{?oa$V_ E3jerY@U^XZ~nSvY/1b,w;$J1f_Suq/&6w!UgMj(<S=y$z}l1BSm' );
define( 'LOGGED_IN_SALT',   '3jW/xS{8f&Q1C$W:7+g>U=r V>|7--5Y2Kmcn@Y/Sgz#r`SQa+naA@k|o^8qG)O:' );
define( 'NONCE_SALT',       'G$/<*6FS]w;gYN}4Yh^Gz4% $L?LDgY5?nGpxjAS&Khot`CGqrWZhR|u!bx5y%[a' );

/* */
define('FS_METHOD', 'direct');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
